import mongoose from 'mongoose';

import postSchema from './postSchema';
import { createConnection } from './connection'

export default class PostDao {
    Post: any;

    constructor() {
        // Create a Post model from postSchema
        // An instance of a model is a document
        this.Post = mongoose.model('Post', postSchema);
    }

    getPost = async (id: string) => {
        const conn = await createConnection();
        return new Promise(async (resolve, reject) => {
            try {
                const post = await this.Post.findById(id);
                resolve(post);
            } catch (err) {
                console.error(`Error during getPostById: ${err}`);
                let error = err.name === 'CastError' ? `Post with id=${id} was not found`: err;
                reject(error);
            } finally {
                (await conn).close();
                console.log('MongoDB connection closed.');
            }
        });
    }

    getAllPosts = async () => {
        const conn = await createConnection();
        return new Promise(async (resolve, reject) => {
            try {
                const posts: object[] = await this.Post.find();
                const response = {
                    length: posts.length,
                    posts,
                }
                resolve(response);
            } catch (err) {
                console.error(`Error during getAllPosts: ${err}`);
                reject(err);
            } finally {
                (await conn).close();
                console.log('MongoDB connection closed.');
            }
        });
    }

    createPost = async (
        title: string,
        author: string,
        body: string
    ) => {
        const conn = await createConnection();
        return new Promise(async (resolve, reject) => {
            try {
                let post = new this.Post({ title, author, body });
                resolve(await post.save());
            } catch (err) {
                console.error(`Error during createPost: ${err}`);
                reject(err);
            } finally {
                (await conn).close();
                console.log('MongoDB connection closed.');
            }
        });
    }

    updatePost = async (id: string, update: object) => {
        const conn = await createConnection();
        return new Promise(async (resolve, reject) => {
            try {
                const post = await this.Post.findByIdAndUpdate(id ,update, { new: true });
                resolve(post);
            } catch (err) {
                console.error(`Error during updatePost: ${err}`);
                reject(err);
            } finally {
                (await conn).close();
                console.log('MongoDB connection closed.');
            }
        });
    }

    deletePost = async (id: string) => {
        const conn = await createConnection();
        return new Promise(async (resolve, reject) => {
            try {
                const post = await this.Post.findByIdAndUpdate(id, { hidden: true }, { new: true });
                resolve(post);
            } catch (err) {
                console.error(`Error during deletePost: ${err}`);
                reject(err);
            } finally {
                (await conn).close();
                console.log('MongoDB connection closed.');
            }
        });
    }
}