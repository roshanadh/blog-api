import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const postSchema = new Schema({
    title: String,
    author: String,
    body: String,
    date: {
        type: Date,
        default: Date.now()
    },
    hidden: {
        type: Boolean,
        default: false
    }
});

export default postSchema;
