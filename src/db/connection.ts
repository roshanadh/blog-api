import mongoose from 'mongoose';

export function createConnection() {
    mongoose.connect('mongodb://localhost:27017/blog', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
    });
    
    const conn = mongoose.connection;
    conn.on('error', err => {
        console.error(`Error in MongoDB connection: ${err}`);
    });
    
    conn.once('open', () => {
        console.log('MongoDB connection established.')
    });
    
    return conn;
}
