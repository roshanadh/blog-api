import express from 'express';
import PostController from '../controllers/postController';

const router = express.Router();
const postController = new PostController();

router.get('/', (req, res) => {
    res.status(200).json({
        message: "Hello World!"
    });
});

router.get('/post/:id', postController.getPostById);
router.get('/posts', postController.getAllPosts);
router.post('/post', postController.createPost);
router.put('/post', postController.updatePost);
router.delete('/post', postController.deletePost);

export default router;
