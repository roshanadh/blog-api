import { Request, Response } from 'express';
import PostDao from '../db/postDao';

const postDao = new PostDao();
export default class PostController {
    getPostById = async (req: Request, res: Response) => {
        const postDao = new PostDao();
        console.log(`Request received at GET /post/${req.params.id}`);

        await postDao
            .getPost(req.params.id)
            .then(post => {
                res.status(200).json({ post });
            })
            .catch(error => {
                res.status(404).json({ error });
            });
        }

    getAllPosts = async (req: Request, res: Response) => {
        const postDao = new PostDao();
        console.log(`Request received at GET /posts`);

        await postDao
            .getAllPosts()
            .then(posts => {
                res
                    .status(200)
                    .json(posts);
            })
            .catch(error => {
                res.status(503).json({ error });
            });
    }

    createPost = async (req: Request, res: Response) => {
        const postDao = new PostDao();
        console.log(`Request received at POST /post`);

        await postDao
            .createPost(req.body.title, req.body.author, req.body.body)
            .then(post => {
                res.status(200).json({ post });
            })
            .catch(error => {
                res.status(503).json({ error });
            });
    }

    updatePost = async (req: Request, res: Response) => {
        const postDao = new PostDao();
        console.log(`Request received at PUT /post`);

        await postDao
            .updatePost(req.body.id, req.body.update)
            .then(post => {
                res.status(200).json({ post });
            })
            .catch(error => {
                res.status(503).json({ error });
            });
    }

    deletePost = async (req: Request, res: Response) => {
        const postDao = new PostDao();
        console.log(`Request received at DELETE /post`);

        await postDao
            .deletePost(req.body.id)
            .then(post => {
                res.status(200).json({ post });
            })
            .catch(error => {
                res.status(503).json({ error });
            });
    }
}