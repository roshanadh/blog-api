import express from 'express';
import * as bodyParser from 'body-parser';

import utils from './utils';
import router from './routes/router';

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(router);

app.listen(utils.PORT, () => {
    console.log(`Server is now listening on port ${utils.PORT}...`);
});
